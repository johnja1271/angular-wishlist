import { v4 as uuid } from 'uuid'

export class DestinoViaje {

    selected = false;
    servicios: string[];
    id = uuid();
    votes = 0;

    constructor(public n: string, public u: string) {
        this.servicios = ['pileta', 'desayuno'];
    }

    setSelected(s: boolean): any {
        
        this.selected = s;
    }

    isSelected(): boolean{
        return this.selected;
    }

    public voteUp(): any {
        this.votes++;
        
    }

    voteDown(): any {
        this.votes--;
    }
}