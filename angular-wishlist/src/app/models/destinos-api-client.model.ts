import { Subject } from 'rxjs';
import { AppConfig, AppState, APP_CONFIG } from '../app.module';
import { DestinoViaje } from './destino-viaje.model';
import { Store } from '@ngrx/store';
import { ELEGIDO_FAVORITO, NUEVO_DESTINO } from './destinos-viajes-state.model';
import { forwardRef, Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { db } from '../app.module';

@Injectable()

export class DestinosApiClient {
    destinos: DestinoViaje[] = [];
    current: Subject<DestinoViaje> = new Subject<DestinoViaje>();

    constructor(
        public store: Store<AppState>,
        @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
        private http: HttpClient) {
        this.store.select(state => state.destinos).subscribe((data) => {
            this.destinos = data.items;
        });
    }

    add(d: DestinoViaje) {
        const headers: HttpHeaders = new HttpHeaders({ 'X-API-TOKEN': 'token-seguridad' });
        const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.n }, { headers: headers });
        this.http.request(req).subscribe((data: any) => {
            if (data.status === 200) {
                this.store.dispatch(NUEVO_DESTINO({ destino: d }));
                const myDb = db;
                myDb.destinos.add(d);
                console.log("todos los destinos de la db");
                myDb.destinos.toArray().then( (destinos: any) => console.log(destinos));
            }
        });
    }

    elegido(d: DestinoViaje) {

        this.store.dispatch(ELEGIDO_FAVORITO({ destino: d }));
    }

    changeSelected(d: DestinoViaje) {
        return this.store.select(state => state.destinos.favorito?.setSelected(true))
    }

    getAll(): DestinoViaje[] {
        return this.destinos
    }

    getById(id: string): (DestinoViaje | null) {
        return null;
    }

}