import { Component, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { AppState } from '../../app.module';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { Store } from '@ngrx/store';
import { ELEGIDO_FAVORITO, VOTE_UP } from '../../models/destinos-viajes-state.model';
import { VOTE_DOWN } from '../../models/destinos-viajes-state.model';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
    ])
  ]
})

export class DestinoViajeComponent implements OnInit {


  @Input() position: number = 0;
  @Input() destino: any;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() clicked: EventEmitter<DestinoViaje>;

  constructor(private store: Store<AppState>) {

    this.clicked = new EventEmitter();

  }

  ngOnInit() {

  }

  ir() {
    this.clicked.emit(this.destino);
    return false
  }

  public voteUp(): boolean {

    this.store.dispatch(VOTE_UP({ destino: this.destino }));
    return false;
  }

  voteDown() {

    this.store.dispatch(VOTE_DOWN({ destino: this.destino }));
    return false;
  }
}
