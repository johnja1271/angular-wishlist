import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AppState } from '../../app.module';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { Store, select } from '@ngrx/store';
import { ELEGIDO_FAVORITO, NUEVO_DESTINO } from '../../models/destinos-viajes-state.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [ DestinosApiClient ]
})
export class ListaDestinosComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  update: string[];
  
  constructor(public destinosApiClient: DestinosApiClient, public store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.update = new Array(); 
    
   }

  ngOnInit(){
    this.store.select(state => state.destinos.favorito).subscribe(d =>{
        if(d != null){
         this.update.push('Se ha elegido a ' + d.n);
      }
    });

  }

  public agregado(d: DestinoViaje){
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  public elegido(e: DestinoViaje){
    this.destinosApiClient.elegido(e);
  }

}
